function playSound(sound) {
  fetch('/sound/' + sound, {
    method: 'POST'
  })
    .then(console.log)
    .catch(console.error);
}

function playMeme(meme) {
  fetch('/meme/' + meme, {
    method: 'POST'
  })
    .then(console.log)
    .catch(console.error);
}

function toggleKeyboard() {
  let result = prompt("How often you want to play this sound?", 50);
  fetch('/keyboard/' + result, {
    method: 'POST'
  })
    .then(console.log)
    .catch(console.error);
}