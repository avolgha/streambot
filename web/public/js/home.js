const body = document.querySelector('body');

const createHomeButton = () => {
  const element = document.createElement('button');
  element.id = 'btn-home';
  element.innerHTML = `<img height="64" width="64" src="../img/home.png" alt="Home Button" />`;
  element.onclick= () => {
    window.location.href = '/dashboard/home';
  };
  return element;
};

body.onload = () => body.appendChild(createHomeButton());