function detectInternetExplorer() {
  const agent = window.navigator.userAgent;
  const msie = agent.indexOf('MSIE ');
  const trident = agent.indexOf('Trident/');

  return msie > 0 || trident > 0 || false;
}

if (detectInternetExplorer()) {
  window.location.replace('/legacy');
}