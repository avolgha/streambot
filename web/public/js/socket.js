const trigger = (() => {
  const element = document.createElement('h1');
  element.innerHTML = 'TRIGGER';
  element.id = 'trigger';
  return element;
})();

const socket = io('http://localhost:' + socketIoPort, {
  extraHeaders: {
    'Access-Control-Allow-Origin': '*'
  }
});

socket.on('action', console.log);

const mainContainer = document.getElementById('container');
socket.on('message', (data) => {
  if (data[0] === 'trigger') {
    actionQueue.push(() => {
      const triggerElement = trigger;
      const [container] = wrapToContainer(triggerElement);

      mainContainer.appendChild(container);
      setTimeout(() => {
        mainContainer.removeChild(container);

        actionQueue.shift();
        const next = actionQueue.peek();
        if (next) {
          next();
        }
      }, data[1]);
    });

    if (actionQueue.length === 1) {
      actionQueue.peek()();
    }
  } else if (data[0] === 'sound') {
    new Audio(`/sounds/${data[1]}.mp3`).play();
  } else if (data[0] === 'meme') {
    actionQueue.push(() => {
      const type = data[1];
      const url = `/memes/${data[2]}`;
      /** @type {HTMLDivElement} */
      let element;
      switch (type) {
        case 'img':
          let tempImg = document.createElement('img');
          tempImg.src = url;
          tempImg.id = 'trigger';
          const [container] = wrapToContainer(tempImg);
          element = container;
          break;
        case 'video':
          let tempVideo = document.createElement('video');
          tempVideo.src = url;
          tempVideo.onended = () => {
            mainContainer.removeChild(element);

            actionQueue.shift();
            const next = actionQueue.peek();
            if (next) {
              next();
            }
          };
          const [container2] = wrapToContainer(tempVideo);
          element = container2;
          setTimeout(() => tempVideo.play(), 500);
          break;
        case 'svg':
          const [container3] = generateContainer();
          container3.innerHTML = data[2];
          element = container3;
          break;
      }
      if (data[3] && data[3] !== undefined && data[3] !== null) {
        element.style = data[3];
      }
      mainContainer.appendChild(element);

      if (type !== 'video') {
        setTimeout(() => {
          mainContainer.removeChild(element);
          actionQueue.shift();
          const next = actionQueue.peek();
          if (next) {
            next();
          }
        }, 5000);
      }
    });

    if (actionQueue.length === 1) {
      actionQueue.peek()();
    }
  } else if (data[0] === 'keyboard') {
    toggleKeyboard(parseInt(data[1]));
  }
});