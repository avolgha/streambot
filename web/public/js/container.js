function genRanNumb(max=0, min=0) {
  return Math.floor(Math.random()*(max - min + 1) + min);
}

const string = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
const idLength = 32;
function generateId() {
  let id = '';
  for (var i = 0; i < idLength; ++i) {
    id += string.split('')[genRanNumb(string.length)];
  }
  return id;
}

function generateContainer() {
  const id = `container-${generateId()}`;
  const element = document.createElement('div');
  element.setAttribute('data-container', '');
  element.id = id;
  return [element, id];
}

function wrapToContainer(child) {
  const id = `container-${generateId()}`;
  const element = document.createElement('div');
  element.setAttribute('data-container', '');
  element.id = id;
  element.appendChild(child);
  return [element, child, id];
}