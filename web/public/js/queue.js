/**
 * Simple queue implementation
 */
class Queue {
  /**
   * Initialize the queue as empty queue
   */
  constructor() {
    this.elements = [];
  }

  /**
   * Pushes a new action to the end of the queue
   * @param {() => void} e 
   */
  push(e) {
    this.elements.push(e);
  }

  /**
   * Removes the first action from the queue and returns it
   */
  shift() {
    return this.elements.shift();
  }

  /**
   * Check if the queue has members in it
   */
  isEmpty() {
    return this.elements.length == 0;
  }

  /**
   * Returns the first element of the queue. If there isn't any, returns `undefined`
   */
  peek() {
    return !this.isEmpty() ? this.elements[0] : undefined;
  }

  /**
   * Returns the length of the queue
   */
  get length() {
    return this.elements.length;
  }
}

const actionQueue = new Queue();