const clickSounds = [
	{
		sounds: [
			new Howl({ src: "click_sounds/click4_1.wav" }),
			new Howl({ src: "click_sounds/click4_11.wav" }),
		],
		counter: 0,
	},
	{
		sounds: [
			new Howl({ src: "click_sounds/click4_2.wav" }),
			new Howl({ src: "click_sounds/click4_22.wav" }),
		],
		counter: 0,
	},
	{
		sounds: [
			new Howl({ src: "click_sounds/click4_3.wav" }),
			new Howl({ src: "click_sounds/click4_33.wav" }),
		],
		counter: 0,
	},
	{
		sounds: [
			new Howl({ src: "click_sounds/click4_4.wav" }),
			new Howl({ src: "click_sounds/click4_44.wav" }),
		],
		counter: 0,
	},
	{
		sounds: [
			new Howl({ src: "click_sounds/click4_5.wav" }),
			new Howl({ src: "click_sounds/click4_55.wav" }),
		],
		counter: 0,
	},
	{
		sounds: [
			new Howl({ src: "click_sounds/click4_6.wav" }),
			new Howl({ src: "click_sounds/click4_66.wav" }),
		],
		counter: 0,
	},
];

function playClick() {
	let rand = Math.floor(Math.random() * clickSounds.length);
  let randomSound = clickSounds[rand];
	randomSound.counter++;
	if (randomSound.counter === 2) randomSound.counter = 0;
  randomSound.sounds[randomSound.counter].seek(0);
  randomSound.sounds[randomSound.counter].play();
}

function toggleKeyboard(times=100) {
  for (var i = 0; i < times; i++) {
    setTimeout(() => playClick(), 150 * i + (times % 3 === 0 ? 10 : 0) + (times % 2 === 0 ? 5 : 0));
  }
}