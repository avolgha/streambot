module.exports = {
  name: 'example',
  id: {
    master: 'avolgha',
    name: 'example',
  },
  version: '1.0.0-alpha',
  author: 'avolgha',
  description: 'example plugin',
  depends: [
  ],
  inject: (_options) => {
    console.log('Called example plugin');
  },
  exports: {}
};