import tmi from 'tmi.js';
import {get as cfg} from './config';

const client = new tmi.Client({
  channels: cfg('channels') || [],
  identity: {
    username: cfg('user.name'),
    password: cfg('user.oauth')
  }
});

client.connect();

export { client };