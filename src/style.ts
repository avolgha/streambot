import choki from 'chokidar';
import sass from 'sass';
import {readdirSync, existsSync, writeFileSync, rmSync, mkdirSync} from 'fs';
import {join, sep} from 'path';

import {defaultLogger as logger} from './logger';

export default function init() {
  const sourceDir = join(__dirname, '..', 'web', 'public', 'scss');
  const outputDir = join(__dirname, '..', 'web', 'public', 'css');

  if (existsSync(outputDir)) {
    rmSync(outputDir, {recursive: true});
    mkdirSync(outputDir);
  }

  readdirSync(sourceDir)
    .forEach((scssFile) => {
      const path = join(sourceDir, scssFile);

      const compiled = sass.renderSync({
        file: path,
        indentType: 'tab'
      });
  
      const file = join(outputDir, path.substring(path.lastIndexOf(sep) + 1).replace('.scss', '.css'));
      
      if (existsSync(file)) rmSync(file);
      writeFileSync(file, compiled.css, {encoding: 'utf-8'});
  
      logger.debug(`Compiled ${path}`);
    })

  choki.watch(sourceDir).on('change', (path) => {
    const compiled = sass.renderSync({
      file: path,
      indentType: 'tab'
    });

    const file = join(outputDir, path.substring(path.lastIndexOf(sep) + 1).replace('.scss', '.css'));
    
    if (existsSync(file)) rmSync(file);
    writeFileSync(file, compiled.css);

    logger.debug(`Compiled ${path}`);
  });
}