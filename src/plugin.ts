import tmi from 'tmi.js';
import express from 'express';
import socket from 'socket.io';
import {readdirSync, rmdirSync} from 'fs';
import {join} from 'path';
import chalk from 'chalk';
import {defaultLogger as logger} from './logger';

export interface PluginGet {
  get<T extends Plugin>(id: Id): T | undefined;
};

export interface PluginInitOptions extends PluginGet {
  client: tmi.Client,
  server: express.Express,
  socket: socket.Socket,
  socketServer: socket.Server
};

export type Id = {
  readonly master: string;
  readonly name: string;
};

export interface Plugin {
  readonly name: string;
  readonly id: Id;
  readonly version: string;
  readonly author: string;
  readonly description?: string;

  readonly depends: Id[];

  inject(options: PluginInitOptions): boolean;

  readonly exports: {[name: string]: [value: (options: PluginGet & {
    [key: string]: [value: any]
  }) => any]}
};

export function compareId(id1: Id, id2: Id) {
  return id1.master === id2.master && id1.name === id2.name;
}

export const pluginMap = new Map<Id, Plugin>();

export function getPlugin(id: Id) {
  return getPlugins().filter(plugin => compareId(plugin.id, id)).at(0);
};

export const dir = join(__dirname, '..', 'plugins');
export const cacheDir = join(__dirname, '..', 'plugins', '.cache');

export function initPlugins(onError: (error: Error, file: string) => void = (error, file) => {
  logger.error(`An error occurred on plugin (${file}):\n${error}`);
}) {
  readdirSync(dir, {encoding: 'utf-8'})
    .filter(file => file.endsWith('.js') || file.endsWith('.coffee'))
    .forEach(file => {
      const ext = file.substring(file.lastIndexOf('.')-1);
      try {
        const module = require(join(__dirname, 'plugins', ext)) as {
          compile(file: string): Plugin
        };

        const plugin = module.compile(file);
        pluginMap.set(plugin.id, plugin);
      } catch (error) {
        onError(error as Error, file);
      }
    });
  
  process.on('beforeExit', () => {
    rmdirSync(cacheDir, {recursive: true});
  });
};

export function injectPlugins(injectOptions: PluginInitOptions) {
  const plugins = getPlugins();
  plugins.forEach(plugin => {
    const needed = plugin.depends.filter(depend => !plugins.some(dP => compareId(dP.id, depend)));
    if (needed.length > 0) {
      logger.error(`Cannot load plugin '${plugin.name}' because these dependencies does not exists: ${chalk.gray(needed.map(id => `${chalk.greenBright(id.master)}${chalk.gray('/')}${chalk.yellowBright(id.name)}`).join(', '))}`);
      return;
    }
    
    plugin.inject(injectOptions);
  });
}

export function getPlugins() {
  const cache: Plugin[] = [];
  const iterator = pluginMap.values();
  while (true) {
    const result = iterator.next();
    if (result.done && result.done === true)
      break;
    else
      cache.push(result.value);
  }
  return cache;
};
