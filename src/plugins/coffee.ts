import Coffee from 'coffeescript';
import {existsSync, mkdirSync, readFileSync, rmSync, writeFileSync} from 'fs';
import {join} from 'path';

import {cacheDir, dir as sourceDir, Plugin} from '../plugin';

export function compile(file: string): Plugin {
  const content = readFileSync(join(sourceDir, file), {encoding: 'utf-8'});
  const compiled = Coffee.compile(content);
  if (!existsSync(cacheDir)) mkdirSync(cacheDir);
  const newPath = join(cacheDir, file.replace('.coffee', '.js'));
  if (existsSync(newPath)) rmSync(newPath, {force: true});
  writeFileSync(newPath, compiled);
  return require(newPath) as Plugin;
}