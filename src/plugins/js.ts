import {Plugin} from '../plugin';

export function compile(file: string): Plugin {
  return require(file) as Plugin;
}