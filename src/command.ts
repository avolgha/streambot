import {Client, ChatUserstate, Badges} from 'tmi.js';
import {get as cfg} from './config';
import {PermissionCheck, checkFor} from './secruity/permissions';

type Callback = () => void;

export interface CommandOptions {
  channel: string;
  self: boolean;
  message: string;
  messageSplit: string[];
  messageType: 'chat' | 'action' | 'whisper';
  user: string;
  badges: Badges;
  color: string;
  mod: boolean;
  subscriber: boolean;
  userType: '' | 'mod' | 'global_mod' | 'admin' | 'staff';

  prefix: string;

  respond(message: string): void;
  whisper(message: string): void;
  usage(commandName: string, message: string): void;

  checkPermission(permission: PermissionCheck, callback?: [Callback?, Callback?]): boolean,

  _userstate: ChatUserstate;
};

export function from(client: Client, channel: string, userstate: ChatUserstate, message: string, self: boolean): CommandOptions {
  return {
    channel,
    message,
    messageSplit: message.split(' '),
    messageType: userstate['message-type'] || 'chat',
    self,
    user: userstate.username || '',
    badges: userstate.badges || {},
    color: userstate.color || '',
    mod: userstate.mod || false,
    subscriber: userstate.subscriber || false,
    userType: userstate['user-type'] || '',

    prefix: cfg('cmd.prefix'),

    respond: (message) => client.say(channel, message),
    whisper: (message) => client.whisper(userstate.username || '', message),
    usage: (name, usage) => {
      if (!userstate.username) return;
      const usageMsg = `@${userstate.username} Usage: ${cfg('cmd.prefix')}${name}`;
      if (usage === '') {
        client.say(channel, usageMsg);
      } else {
        client.say(channel, `${usageMsg} ${usage}`)
          .catch(console.error);
      }
    },

    checkPermission: (permission, callback) => {
      const state = checkFor(permission, userstate);
      if (callback) {
        const [success, failure] = callback;
        if (success && state === true) {
          success();
        } else if (failure && state === false) {
          failure();
        }
      }
      return state;
    },

    _userstate: userstate
  };
}