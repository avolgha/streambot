import tmi from 'tmi.js';

export type PermissionCheck = 'regular' | 'sub' | 'vip' | 'mod' | 'vip+mod' | 'admin';

// TODO: check for VIP status
export function checkFor(permission: PermissionCheck, user: tmi.Userstate) {
  if (user.badges?.broadcaster === '1') return true;

  switch (permission) {
    case 'regular':
      return true;
    case 'sub':
      return user.subscriber || false;
    case 'vip':
      return user.badges?.premium === '1' || false;
    case 'vip+mod':
      return user.mod || user.badges?.premium === '1' || false;
    case 'mod':
      return user.mod || false;
  }

  return false;
}