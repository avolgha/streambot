import {white, bgGreenBright, bgRedBright, bgYellowBright, bgBlueBright} from 'chalk';

export class Logger {
  print(message: string, prefix: string, stream: NodeJS.WriteStream = process.stdout) {
    stream.write(`${prefix} ${white(message)}\n`);
  }

  log(message: string) {
    this.print(message, bgGreenBright(' INFO '));
  }

  debug(message: string) {
    this.print(message, bgBlueBright(' DEBUG '));
  }

  warn(message: string) {
    this.print(message, bgYellowBright(' WARN '));
  }

  error(message: string) {
    this.print(message, bgRedBright(' ERROR '), process.stderr);
  }
};

export const defaultLogger = new Logger();