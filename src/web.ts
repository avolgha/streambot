import express from 'express';
import expressCors from 'cors';
import sio from 'socket.io';
import siot from 'socket.io/dist/typed-events';
import {join} from 'path';
import {existsSync, readdirSync, readFileSync} from 'fs';
import {marked} from 'marked';
const webDir = join(__dirname, '..', 'web');

import {defaultLogger as logger} from './logger';
import {get as cfg} from './config';

marked.setOptions({
  renderer: new marked.Renderer(),
  highlight: function(code, lang) {
    const hljs = require('highlight.js');
    const language = hljs.getLanguage(lang) ? lang : 'plaintext';
    return hljs.highlight(code, { language }).value;
  }
});

const socketIoPort = cfg('server.socketIo') as number || 9999;
const socketServer = new sio.Server(socketIoPort, {
  cors: {
    methods: [ 'GET' ]
  }
});

let socket: sio.Socket<siot.DefaultEventsMap, siot.DefaultEventsMap, siot.DefaultEventsMap, any>;
socketServer.on('connection', (payloadSocket) => {
  socket = payloadSocket;
});

const app = express();

app.use(expressCors({
  origin: 'http://localhost'
}));
app.use(express.static(join(webDir, 'public')));
app.set('views', join(webDir, 'views'));
app.set('view engine', 'ejs');

app.get('/', (_req, res) => res.render('index', {
  socketIoPort,
}));

export const getMemes = () => readdirSync(join(__dirname, '..', 'web', 'public', 'memes'))
.filter(file => {
  switch (file.substring(file.lastIndexOf('.')+1)) {
    case 'png': 
    case 'jpg': case 'jpeg': 
    case 'gif': 
    case 'mp4':
    case 'svg':
      return true;
    default:
      return false;
  }
});

app.get('/legacy', (_req, res) => res.render('legacy'));

app.get('/dashboard', (_req, res) => res.redirect('/dashboard/home'));

app.get('/dashboard/:name', (req, res) => {
  const {name} = req.params;
  const file = join(__dirname, '..', 'web', 'views', 'dashboard', `${name}.ejs`);
  if (existsSync(file)) {
    let params: any = {};
    if (name === 'sounds') {
      params.sounds = readdirSync(join(__dirname, '..', 'web', 'public', 'sounds'))
      .filter(file => file.endsWith('.mp3'))
      .map(file => {
        return {
          name: file.substring(0, file.lastIndexOf('.')),
          file
        }
      });
    } else if (name === 'memes') {
      params.memes = getMemes().map(file => {
        return {
          name: file.substring(0, file.lastIndexOf('.')),
          file
        }
      });
    }
    res.render(`dashboard/${name}`, params);
  } else {
    res.render(`dashboard/home`);
  }
});

app.get('/readme', (_req, res) => {
  let parsed: string | undefined = undefined;
  
  if (existsSync(join(__dirname, '..', 'README.md'))) {
    parsed = marked(readFileSync(join(__dirname, '..', 'README.md'), {encoding: 'utf-8'}));
  }
  
  res.send(`
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ReadME</title>

  <link rel="stylesheet" href="dracula.css">

  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet"> 

  <style>
    body {
      font-family: 'Poppins', sans-serif;
    }

    code {
      font-family: 'Consolas';
    }

    a {
      color: rgb(88, 166, 255);
      text-decoration: none;
    }

    a:hover {
      text-decoration: underline;
    }
  </style>
</head>
<body>
${parsed || '<p style="color: red;">Could not parse any README file</p>'}
</body>
</html>
`);
});
                        
app.post('/sound/:name', (req, res) => {
  socket.emit('message', ['sound', req.params.name]);
  res.status(200).send('Success');
});

app.post('/meme/:name', (req, res) => {
  let name = req.params.name;
  const ext = name.substring(name.lastIndexOf('.')+1);

  let metaFileName = join(__dirname, '..', 'web', 'public', 'memes', `${name.substring(0, name.lastIndexOf('.'))}.meta.json`);
  let customCss = undefined;
  if (existsSync(metaFileName)) {
    const meta = JSON.parse(readFileSync(metaFileName, {encoding: 'utf-8'})) as {
      usable?: boolean,
      css?: string,
    };
    if (meta.usable !== undefined && meta.usable === false) {
      res.status(200).send('Unusable meme');
      return;
    }

    if (meta.css !== undefined && typeof meta.css === 'string') {
      customCss = meta.css;
    }    
  }

  if (ext) {
    if (ext === 'mp4') {
      const payload = ['meme', 'video', name];
      if (customCss !== undefined) payload.push(customCss);
      socket.emit('message', payload);
    } else if (ext === 'svg') {
      const payload = ['meme', 'svg', name];
      if (customCss !== undefined) payload.push(customCss);
      socket.emit('message', payload);
    } else {
      const payload = ['meme', 'img', name];
      if (customCss !== undefined) payload.push(customCss);
      socket.emit('message', payload);
    }
  }
  
  res.status(200).send('Success');
});

app.post('/keyboard/:times', (req, res) => {
  socket.emit('message', ['keyboard', req.params.times]);
  res.status(200).send('Success'); 
});

export function start() {
  app.listen(cfg('server.express') || 9999, () => logger.log('Started Web Server'));
}

export function sendRequest(data: any[]) {
  socket.broadcast.emit('action', data);
}

export { socket, socketServer, app };