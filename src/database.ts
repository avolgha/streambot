import {readFileSync, readdirSync, statSync} from 'fs';
import {join} from 'path';
import {walk} from './util';

export type DatabaseType = 'file-json' | 'directory';

export type Filter = (inputFile: string) => boolean;

export interface Database {};

export interface JsonDatabase {
  get content(): string;
  get parsed(): any;
};

export interface DirectoryDatabase {
  reload(): void;
  filter(filter: Filter): void;
  collect(): string[];
};

export interface JsonOptions {
  file: string;
};

export interface DirectoryOptions {
  dir: string;
  deep: boolean;
};

export function createDatabase(type: 'file-json', options: JsonOptions): Database & JsonDatabase;
export function createDatabase(type: 'directory', options: DirectoryOptions): Database & DirectoryDatabase;
export function createDatabase(type: DatabaseType, options: {}): Database {
  if (type === 'file-json') {
    const database: Database & JsonDatabase = {
      get content() {
        return readFileSync((options as JsonOptions).file, {encoding: 'utf-8'});
      },

      get parsed() {
        return JSON.parse(this.content);
      }
    };
    return database;
  } else if (type === 'directory') {
    let opt = options as DirectoryOptions;
    class DirDatabaseImpl implements Database, DirectoryDatabase {
      private dir: string;
      private files: string[] = [];

      constructor() {
        this.dir = opt.dir;
        if (opt.deep) {
          this.files = walk(this.dir);
        } else {
          this.files = readdirSync(this.dir)
            .filter(file => statSync(file).isFile())
            .map(file => join(this.dir, file));
        }
      }

      reload(): void {
        if (opt.deep) {
          this.files = walk(this.dir);
        } else {
          this.files = readdirSync(this.dir)
            .filter(file => statSync(file).isFile())
            .map(file => join(this.dir, file));
        }
      }

      filter(filter: Filter): void {
        const newArray: string[] = [];
        this.files
          .filter(filter)
          .forEach(file => newArray.push(file));
        this.files = newArray;
      }

      collect(): string[] {
        return this.files;
      }
    }
    return new DirDatabaseImpl();
  }
  return {
    reload() {
      throw new Error('Method not implemented');
    }
  };
};