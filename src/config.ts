import {readFileSync} from 'fs';
import {join} from 'path';

let config: any;

export { config };

export function initialize() {
  const fileContent = readFileSync(join(__dirname, '..', 'config.json'), {encoding: 'utf-8'});
  config = JSON.parse(fileContent);
}

export function get(key: string) {
  let current = config;
  let index = 0;
  for (const element of key.split('.')) {
    if (index === key.split('.').length - 1)
      return current[element];

    current = current[element];
    index++;
  }
}