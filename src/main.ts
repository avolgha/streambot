import {initialize, get as cfg} from './config'; initialize();

import {existsSync as exists} from 'fs';
import {join} from 'path';
import chalk from 'chalk';

import {defaultLogger as logger} from './logger';

import {client} from './twitch';
import {start, app, socket, socketServer} from './web';
import {initPlugins, getPlugin, Id, Plugin, getPlugins, injectPlugins} from './plugin';
import {from} from './command';
import initScss from './style';

initPlugins();

// print a information menu
(() => {
  console.clear();

  const print = (msg: string) => console.log(msg);
  const pj = require(join(__dirname, '..', 'package.json'));

  print("\n\n");
  print(chalk.gray('>\tRunning StreamBot v') + chalk.greenBright(pj.version));
  print(chalk.gray('>\tBot created by ') + chalk.magentaBright(pj.author));
  print(chalk.gray('>\tLicensed under ') + chalk.redBright(pj.license));
  print(chalk.gray(`>\tLoaded ${chalk.yellowBright(getPlugins().length)} plugins`));
  print("\n\n");

  injectPlugins({
    client,
    server: app,
    socket,
    socketServer,
    get<T extends Plugin>(id: Id) {
      const plugin = getPlugin(id);
      if (!plugin)
        return undefined;
      return plugin as T;
    }
  });
})();

initScss();

client.on('connected', () => {
  logger.log('Connected to twitch');
  start();
});

client.on('message', (channel, userstate, message, self) => {
  if (message.length > 0 && message.startsWith(cfg('cmd.prefix'))) {
    const split = message.split(' ');
    const command = split[0].substring(cfg('cmd.prefix').length);
    const file = join(__dirname, 'commands', `${command}.js`);

    if (exists(file)) {
      require(file).default(from(client, channel, userstate, message, self));
    } else {
      if (userstate.username)
        client.whisper(userstate.username, `Hey, the command '${command}' does not exists. Please use ${cfg('cmd.prefix')}help for a list of commands.`)
          .catch(error => logger.error(`Error occurred while whispering:\n${error}`));
    }
  }
});

process.on('exit', () => {
  client.disconnect();
  console.clear();
});