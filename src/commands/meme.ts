import {readFileSync, existsSync} from 'fs';
import {join} from 'path';
import {CommandOptions} from '../command';
import {socket, getMemes} from '../web';

export default function MemeCommand(context: CommandOptions) {
  try {
    if (context.messageSplit.length === 1) {
      context.usage('meme', '<Meme>');
      return;
    }
  
    context.checkPermission('vip+mod', [() => {
      if (context.messageSplit.length === 2 && context.messageSplit[1].toLowerCase() === 'list') {
        const sounds = getMemes()
          .map(file => file.substring(0, file.lastIndexOf('.')));
        context.respond(`Available memes: '${sounds.join('\', \'')}'`);
        return;
      }

      let memeName = '';
      for (let i = 1; i < context.messageSplit.length; i++)
      memeName += context.messageSplit[i] + ' ';
      memeName = memeName.slice(0, memeName.length - 1);

      const available = getMemes()
        .map(file => {
          return {
            name: file.substring(0, file.lastIndexOf('.')),
            file
          };
        });

      if (available.some(({name}) => {
        return memeName.toLowerCase() === name.toLowerCase();
      })) {
        const meme = available.filter(({name}) => memeName.toLowerCase() === name.toLowerCase())[0].file;
        if (!meme) return;
        const ext = meme.substring(meme.lastIndexOf('.')+1);

        let metaFileName = join(__dirname, '..', '..', 'web', 'public', 'memes', `${meme.substring(0, meme.lastIndexOf('.'))}.meta.json`);
        let customCss = undefined;
        if (existsSync(metaFileName)) {
          const meta = JSON.parse(readFileSync(metaFileName, {encoding: 'utf-8'})) as {
            usable?: boolean,
            css?: string,
          };
          if (meta.usable !== undefined && meta.usable === false) {
            context.respond('error: you are trying to execute an unexecutable meme');
            return;
          }
      
          if (meta.css !== undefined && typeof meta.css === 'string') {
            customCss = meta.css;
          }    
        }
      
        if (ext) {
          if (ext === 'mp4') {
            const payload = ['meme', 'video', meme];
            if (customCss !== undefined) payload.push(customCss);
            socket.emit('message', payload);
          } else if (ext === 'svg') {
            const payload = ['meme', 'svg', meme];
            if (customCss !== undefined) payload.push(customCss);
            socket.emit('message', payload);
          } else {
            const payload = ['meme', 'img', meme];
            if (customCss !== undefined) payload.push(customCss);
            socket.emit('message', payload);
          }
        }

        console.log({ext, metaFileName, customCss});
        

        context.respond(`Playing meme '${meme}'`);
      } else {
        context.respond('error: this meme does not exists');
      }
    }]);
  } catch (e) {
    console.error(e);
  }
}