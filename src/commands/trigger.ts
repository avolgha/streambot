import {CommandOptions} from '../command';
import {socket} from '../web';

export default function TriggerCommand(context: CommandOptions) {
  try {
    if (context.messageSplit.length !== 2) {
      context.usage('trigger', '<Seconds>');
      return;
    }

    context.checkPermission('mod', [() => {
      socket.emit('message', ['trigger', parseInt(context.messageSplit[1]) * 1000]);
    }]);
  } catch(e) {
    console.error(e);
  }
}