import {readdirSync} from 'fs';
import {join} from 'path';
import {CommandOptions} from '../command';
import {socket} from '../web';

export default function SoundCommand(context: CommandOptions) {
  try {
    if (context.messageSplit.length === 1) {
      context.usage('sound', '<Sound>');
      return;
    }
  
    context.checkPermission('vip+mod', [() => {
      if (context.messageSplit.length === 2 && context.messageSplit[1].toLowerCase() === 'list') {
        const sounds = readdirSync(join(__dirname, '..', '..', 'web', 'public', 'sounds'))
          .filter(file => file.endsWith('.mp3'))
          .map(file => file.substring(0, file.lastIndexOf('.')));
        context.respond(`Available sounds: '${sounds.join('\', \'')}'`);
        return;
      }
      
      let sound = '';
      for (let i = 1; i < context.messageSplit.length; i++)
        sound += context.messageSplit[i] + ' ';
      sound = sound.slice(0, sound.length - 1);

      const available = readdirSync(join(__dirname, '..', '..', 'web', 'public', 'sounds'))
        .filter(file => file.endsWith('.mp3'))
        .map(file => {
          return {
            name: file.substring(0, file.lastIndexOf('.')),
            file
          }
        });
      
      if (available.some(({name}) => {
        return sound.toLowerCase() === name.toLowerCase();
      })) {
        socket.emit('message', ['sound', sound]);
        context.respond(`Playing sound '${sound}'`);
      } else {
        context.respond('error: this sound does not exists');
      }
    }]);
  } catch (e) {
    console.error(e);
  }
}