import {readdirSync, statSync} from 'fs';
import {join} from 'path';

export function walk(dir: string): string[] {
  let files: string[] = [];
  readdirSync(dir)
    .forEach(file => {
      if (statSync(join(dir, file)).isFile()) {
        files.push(join(dir, file));
      } else {
        files = [...files, ...walk(join(dir, file))];
      }
    });
  return files;
}