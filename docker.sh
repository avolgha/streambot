#!/bin/bash

echo "Building container..."
docker build -t registry.gitlab.com/avolgha/streambot . >& /dev/null

echo "Pushing container..."
docker push registry.gitlab.com/avolgha/streambot >& /dev/null
