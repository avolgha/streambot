# Stream Bot

A stream bot that nobody needs lol

## Setup

To setup the bot, first install the packages. I use [yarn](https://yarnpkg.com/) as my NodeJS package manager

````bash
# use this if your package manager is yarn
$ yarn

# otherwise do this
$ npm install
````

After that you have to create a configuration file for the bot. Take a look at `config.example.json`. There you will find the options you need to rewrite.

Then simply run

````bash
$ yarn dev
# or
$ npm run dev
````

In your OBS, StreamLabs or whatever you use, you can add a browser source. As url take first `http://localhost:` and then the port you specified in the configuration file as `express`.

If you want to, you can try to access the `/readme` endpoint in the web. If this works, your app should be ready to go :)

## Features



## Support

If you have any questions left, please write me on my discord `Marius#0686` or open an issue